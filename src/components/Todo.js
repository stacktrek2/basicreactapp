import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPenToSquare } from '@fortawesome/free-solid-svg-icons'
import { faTrash } from '@fortawesome/free-solid-svg-icons'

export const Todo = ({task, toggleComplete, deleteTodo, editTodo}) => {
  return (
    <div className='Todo'>

        <div>
        <input type="checkbox" checked={task.completed} onChange={() => toggleComplete(task.id)} readOnly={task.completed} className={`${task.completed ? 'completed' : ""}`}/>
        <label id="labelName">{task.task}</label>
        </div>
        <div>
            <FontAwesomeIcon icon={faPenToSquare} onClick={() => editTodo(task.id)} />
            <FontAwesomeIcon icon={faTrash} onClick={() => deleteTodo(task.id)} className={`${task.completed ? 'completed' : 'icon-unclickable'}`} />
        </div>
    </div>

    
    
  )
}